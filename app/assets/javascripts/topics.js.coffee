class window.Topic
  constructor: ->
    @clickOnTopicContent()
    @clickOnRightSideBtnAllQuestion()
    @clickOnRightSideBtnAllTopics()


  clickOnTopicContent: ->
    $(document).on "click", ".btn_expend_question_content",  ->
      current_element = $(this).closest(".body")
      current_element.find(".question_content").toggle("slow")
      if $(this).text() == "Detail"
        $(this).text("Collapse")
      else
        $(this).text("Detail")
      return false

  clickOnRightSideBtnAllQuestion: -> 
    $(document).on "click", ".btn_all_questions",  ->
      $("#loading").removeClass "hide"
      $.ajax
        type: "GET"
        url: "/topics/all_questions"
        success: (data)->
          $("#loading").addClass "hide"
          $("#right_side_top_questions").html data
        error: (error)->
          $("#loading").addClass "hide"
          alert "Data error!"
      return false
  clickOnRightSideBtnAllTopics: ->
    $(document).on "click", ".btn_all_topics", ->
      $("#loading").removeClass "hide"
      current_topic_temp = $("#current_topic").val()
      $.ajax
        type: "GET"
        url: "/topics/all_topics"
        data: (current_topic: current_topic_temp)
        success: (data) ->
          $("#loading").addClass "hide"
          $("#right_side_all_topics").html data
        error: (error) ->
          $("#loading").addClass "hide"
          alert "Data error!"
      return false
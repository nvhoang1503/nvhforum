json.array!(@questions) do |question|
  json.extract! question, :id, :topic_id, :user_id, :title, :content, :attachment, :status
  json.url question_url(question, format: :json)
end

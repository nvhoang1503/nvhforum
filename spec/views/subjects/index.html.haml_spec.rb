require 'rails_helper'

RSpec.describe "subjects/index", :type => :view do
  before(:each) do
    assign(:subjects, [
      Subject.create!(
        :name => "Name",
        :description => "Description",
        :status => 1
      ),
      Subject.create!(
        :name => "Name",
        :description => "Description",
        :status => 1
      )
    ])
  end

  it "renders a list of subjects" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end

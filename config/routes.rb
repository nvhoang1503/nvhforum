Rails.application.routes.draw do

  resources :answers do
    collection do 
      get "post_answer_feedback"
    end
  end

  resources :questions

  resources :subjects

  get 'home/index'
  root 'home#index'
  
  resources :categories

  resources :topics do
    collection do 
      get "forums"
      get "all_questions"
      get "all_topics"
    end
  end

  resources :profiles

  resources :comments

  devise_for :users
  
  resources :users do
    collection do 
      get "members"
    end
  end

  # get 'users/index'

end

require 'rails_helper'

RSpec.describe "topics/new", :type => :view do
  before(:each) do
    assign(:topic, Topic.new(
      :user_id => 1,
      :category_id => 1,
      :title => "MyString",
      :content => "MyText",
      :attachment => "MyString",
      :status => 1,
      :point => "MyString"
    ))
  end

  it "renders new topic form" do
    render

    assert_select "form[action=?][method=?]", topics_path, "post" do

      assert_select "input#topic_user_id[name=?]", "topic[user_id]"

      assert_select "input#topic_category_id[name=?]", "topic[category_id]"

      assert_select "input#topic_title[name=?]", "topic[title]"

      assert_select "textarea#topic_content[name=?]", "topic[content]"

      assert_select "input#topic_attachment[name=?]", "topic[attachment]"

      assert_select "input#topic_status[name=?]", "topic[status]"

      assert_select "input#topic_point[name=?]", "topic[point]"
    end
  end
end

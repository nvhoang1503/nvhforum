class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :name
      t.text :description
      t.integer :user_id
      t.integer :status

      t.timestamps
    end
  end
end

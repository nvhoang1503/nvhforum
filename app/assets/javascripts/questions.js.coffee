class window.Question
  constructor: ->
    @clickOnExpandTopicContent()


  clickOnExpandTopicContent: ->
    $(document).on "click", ".btn_expand_topic_content",  ->
      
      current_element = $(this).closest(".topic_info")
      current_element.find(".topic_content").toggle("slow")
      
      if $(this).text() == "Expand"
        $(this).text("Collapse")
      else
        $(this).text("Expand")
      return false
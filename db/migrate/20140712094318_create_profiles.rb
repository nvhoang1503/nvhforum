class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id
      t.string :last_name
      t.string :first_name
      t.string :birthday
      t.string :address
      t.string :avatar
      t.string :course
      t.integer :gender
      t.text :resume

      t.timestamps
    end
  end
end

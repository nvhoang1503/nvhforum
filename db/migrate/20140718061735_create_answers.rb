class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.integer :question_id
      t.integer :user_id
      t.text :content
      t.string :attachment
      t.integer :status

      t.timestamps
    end
  end
end

class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.integer :topic_id
      t.integer :user_id
      t.string :title
      t.text :content
      t.string :attachment
      t.integer :status

      t.timestamps
    end
  end
end

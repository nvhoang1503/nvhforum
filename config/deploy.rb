# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'nvhforum'
# set :repo_url, 'git@example.com:me/my_repo.git'
set :repo_url, 'git@bitbucket.org:nvhoang1503/nvhforum.git'
set :branch, "trunk"
# set :user, 'vps22'
# set :password, 'Vdo@12345^'
# # set :password, ask('Server password', nil)
# server '210.245.91.22', user: 'vps22', port: 22, password: "Vdo@12345^", roles: %w{web app db}
# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
# set :deploy_to, '/var/www/my_app'
# set :deploy_to, 'deploy'
# set :deploy_to, "/home/#{fetch(:deploy_user)}/apps/#{fetch(:full_app_name)}"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
# set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      # execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  task :symlink_configs do
    # run "rm #{latest_release}/config/database.yml"
    run %( cd #{latest_release} &&
      ln -nfs #{latest_release}/config/#{env}_database.yml #{latest_release}/config/database.yml
    )
  end

  after :publishing, :restart, :symlink_configs

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end
end

require 'rails_helper'

RSpec.describe "profiles/new", :type => :view do
  before(:each) do
    assign(:profile, Profile.new(
      :user_id => 1,
      :last_name => "MyString",
      :first_name => "MyString",
      :birthday => "MyString",
      :address => "MyString",
      :avatar => "MyString",
      :course => "MyString",
      :gender => 1,
      :resume => "MyText"
    ))
  end

  it "renders new profile form" do
    render

    assert_select "form[action=?][method=?]", profiles_path, "post" do

      assert_select "input#profile_user_id[name=?]", "profile[user_id]"

      assert_select "input#profile_last_name[name=?]", "profile[last_name]"

      assert_select "input#profile_first_name[name=?]", "profile[first_name]"

      assert_select "input#profile_birthday[name=?]", "profile[birthday]"

      assert_select "input#profile_address[name=?]", "profile[address]"

      assert_select "input#profile_avatar[name=?]", "profile[avatar]"

      assert_select "input#profile_course[name=?]", "profile[course]"

      assert_select "input#profile_gender[name=?]", "profile[gender]"

      assert_select "textarea#profile_resume[name=?]", "profile[resume]"
    end
  end
end

require 'rails_helper'

RSpec.describe "profiles/show", :type => :view do
  before(:each) do
    @profile = assign(:profile, Profile.create!(
      :user_id => 1,
      :last_name => "Last Name",
      :first_name => "First Name",
      :birthday => "Birthday",
      :address => "Address",
      :avatar => "Avatar",
      :course => "Course",
      :gender => 2,
      :resume => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Last Name/)
    expect(rendered).to match(/First Name/)
    expect(rendered).to match(/Birthday/)
    expect(rendered).to match(/Address/)
    expect(rendered).to match(/Avatar/)
    expect(rendered).to match(/Course/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/MyText/)
  end
end

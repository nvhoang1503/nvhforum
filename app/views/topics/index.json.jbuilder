json.array!(@topics) do |topic|
  json.extract! topic, :id, :user_id, :category_id, :title, :content, :attachment, :status, :point
  json.url topic_url(topic, format: :json)
end

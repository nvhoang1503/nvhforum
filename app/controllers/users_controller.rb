class UsersController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    # @users = User.where("role != ? and id != ?", User::ROLE[:super_admin], current_user.id)
    @q = User.order("role asc, updated_at DESC").page(params[:page]).search(params[:q])
    @users = @q.result
  end

  def members
    @users = User.where("role != ? and id != ?", User::ROLE[:super_admin], current_user.id)
  end

  def show
    @comment = Comment.new
    @comments = @user.profile.comments if @user.profile
  end

  def edit
  end

  def update
    if @user.update(user_params)
      redirect_to @user, notice: 'User was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @user = User.find params[:id]
    @user.destroy
    redirect_to users_path, notice: 'User was successfully deleted.'
    # if @store.has_products?
    #   redirect_to admin_stores_path, alert: "Could not delete the store, it has already had product(s)"
    # else
    #   @store.destroy
    #   redirect_to admin_stores_path, notice: 'Store was successfully deleted.'
    # end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:email, :first_name, :last_name, :avatar, :birthday, :student_of, :address, :status, :role, :resume, :has_profile)
    end

end

class RemoveAttachmentTopicQuestionAnswer < ActiveRecord::Migration
  def change
    remove_column :topics, :attachment
    remove_column :questions, :attachment
    remove_column :answers, :attachment
  end
end

require 'rails_helper'

RSpec.describe "topics/index", :type => :view do
  before(:each) do
    assign(:topics, [
      Topic.create!(
        :user_id => 1,
        :category_id => 2,
        :title => "Title",
        :content => "MyText",
        :attachment => "Attachment",
        :status => 3,
        :point => "Point"
      ),
      Topic.create!(
        :user_id => 1,
        :category_id => 2,
        :title => "Title",
        :content => "MyText",
        :attachment => "Attachment",
        :status => 3,
        :point => "Point"
      )
    ])
  end

  it "renders a list of topics" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Attachment".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Point".to_s, :count => 2
  end
end

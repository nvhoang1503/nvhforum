require 'rails_helper'

RSpec.describe "subjects/show", :type => :view do
  before(:each) do
    @subject = assign(:subject, Subject.create!(
      :name => "Name",
      :description => "Description",
      :status => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
    expect(rendered).to match(/1/)
  end
end

class RemoveTopicIdInComment < ActiveRecord::Migration
  def change
    remove_column :comments, :topic_id
  end
end

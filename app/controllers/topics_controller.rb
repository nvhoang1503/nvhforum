class TopicsController < ApplicationController
  before_action :set_topic, only: [:show, :edit, :update, :destroy]
  before_filter :authenticate_user!, :except => [:forums]

  # GET /topics
  # GET /topics.json
  def index
    # @topics = Topic.all
    @q = Topic.order("updated_at DESC").page(params[:page]).search(params[:q])
    @topics = @q.result
  end

  # GET /topics/1
  # GET /topics/1.json
  def show
  end

  # GET /topics/new
  def new
    @topic = Topic.new
    @topic.attachments.build
    @categories = Category.all
  end

  # GET /topics/1/edit
  def edit
    @categories = Category.all
  end

  # POST /topics
  # POST /topics.json
  def create
    @topic = Topic.new(topic_params)

    respond_to do |format|
      if @topic.save
        format.html { redirect_to @topic, notice: 'Topic was successfully created.' }
        format.json { render :show, status: :created, location: @topic }
      else
        format.html { render :new }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /topics/1
  # PATCH/PUT /topics/1.json
  def update
    respond_to do |format|
      if @topic.update(topic_params)
        format.html { redirect_to @topic, notice: 'Topic was successfully updated.' }
        format.json { render :show, status: :ok, location: @topic }
      else
        format.html { render :edit }
        format.json { render json: @topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /topics/1
  # DELETE /topics/1.json
  def destroy
    @topic.destroy
    respond_to do |format|
      format.html { redirect_to topics_url, notice: 'Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def forums
    if params[:topic_id]
      @topic = Topic.find(params[:topic_id])
    elsif params[:category_id]
      @topic = Topic.top_question_on_category(params[:category_id])
    else 
      @topic = Topic.top_question
    end
    @categories = Category.all
    @topics = Topic.another_topic_in_category(@topic.id, @topic.category_id).limit(2) if @topic 
    @questions = @topic.questions.order("created_at desc") if @topic
    @top_questions = Question.order("created_at desc").limit(5)
  end


  def all_questions
    @questions = Question.all
    render :partial => "topics/right_side_top_question", :locals => {:top_questions => @questions}
  end

  def all_topics
    @topic = Topic.find(params[:current_topic])
    @topics = Topic.another_topic_in_category(@topic.id, @topic.category_id) if @topic
    render :partial => "topics/right_side_another_topics", :locals => {:topics => @topics}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_topic
      @topic = Topic.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def topic_params
      params.require(:topic).permit(:user_id, :category_id, :title, :content, :status, :point, attachments_attributes: [:name, :data, :topic_id])
    end
end

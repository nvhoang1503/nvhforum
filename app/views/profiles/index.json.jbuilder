json.array!(@profiles) do |profile|
  json.extract! profile, :id, :user_id, :last_name, :first_name, :birthday, :address, :avatar, :course, :gender, :resume
  json.url profile_url(profile, format: :json)
end

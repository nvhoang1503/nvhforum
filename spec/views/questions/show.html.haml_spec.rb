require 'rails_helper'

RSpec.describe "questions/show", :type => :view do
  before(:each) do
    @question = assign(:question, Question.create!(
      :topic_id => 1,
      :user_id => 2,
      :title => "Title",
      :content => "MyText",
      :attachment => "Attachment",
      :status => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Attachment/)
    expect(rendered).to match(/3/)
  end
end

require 'rails_helper'

RSpec.describe "comments/edit", :type => :view do
  before(:each) do
    @comment = assign(:comment, Comment.create!(
      :user_id => 1,
      :topic_id => 1,
      :profile_id => 1,
      :content => "MyText",
      :status => 1
    ))
  end

  it "renders the edit comment form" do
    render

    assert_select "form[action=?][method=?]", comment_path(@comment), "post" do

      assert_select "input#comment_user_id[name=?]", "comment[user_id]"

      assert_select "input#comment_topic_id[name=?]", "comment[topic_id]"

      assert_select "input#comment_profile_id[name=?]", "comment[profile_id]"

      assert_select "textarea#comment_content[name=?]", "comment[content]"

      assert_select "input#comment_status[name=?]", "comment[status]"
    end
  end
end

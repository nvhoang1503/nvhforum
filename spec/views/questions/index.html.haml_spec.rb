require 'rails_helper'

RSpec.describe "questions/index", :type => :view do
  before(:each) do
    assign(:questions, [
      Question.create!(
        :topic_id => 1,
        :user_id => 2,
        :title => "Title",
        :content => "MyText",
        :attachment => "Attachment",
        :status => 3
      ),
      Question.create!(
        :topic_id => 1,
        :user_id => 2,
        :title => "Title",
        :content => "MyText",
        :attachment => "Attachment",
        :status => 3
      )
    ])
  end

  it "renders a list of questions" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Attachment".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end

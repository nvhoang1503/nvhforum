class Question < ActiveRecord::Base
  belongs_to :user
  belongs_to :topic
  has_many :answers, dependent: :destroy
  has_many :attachments, dependent: :destroy

  accepts_nested_attributes_for :attachments, :reject_if => :all_blank , :allow_destroy => true
  
end

module UsersHelper
  def current_user_avatar(user)
    if user.profile && user.profile.avatar
      image_tag(user.profile.avatar)
    else
      image_tag("common/user.png")
    end
  end
end

class AddUserInfoToUser < ActiveRecord::Migration
  def change
  	add_column :users, :status, :integer
  	add_column :users, :rule, :integer, :default => User::ROLE[:member]
  end
end

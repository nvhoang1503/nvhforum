require 'rails_helper'

RSpec.describe "topics/show", :type => :view do
  before(:each) do
    @topic = assign(:topic, Topic.create!(
      :user_id => 1,
      :category_id => 2,
      :title => "Title",
      :content => "MyText",
      :attachment => "Attachment",
      :status => 3,
      :point => "Point"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/1/)
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/Attachment/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Point/)
  end
end

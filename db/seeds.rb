puts "============Create supper admin account============="
unless User.exists?(:email => "super_admin@nvhforum.com")
  User.create({
             :email => "super_admin@nvhforum.com",
             :password => "1234qwer",
             :password_confirmation => "1234qwer",
             :role => User::ROLE[:super_admin]
           })
else
  admin = User.find_by_email("super_admin@nvhforum.com")
  admin.update_attribute(:role,User::ROLE[:super_admin])
end

unless User.exists?(:email => "admin@nvhforum.com")
  User.create({
             :email => "admin@nvhforum.com",
             :password => "1234qwer",
             :password_confirmation => "1234qwer",
             :role => User::ROLE[:admin]
           })
else
  admin = User.find_by_email("admin@nvhforum.com")
  admin.update_attribute(:role,User::ROLE[:admin])
end


puts "=============== Create sample data for subject of category ==============="
unless Subject.exists?(:name => "Software")
  Subject.create({
             :name => "Software",
             :description => "Something about Software technical"
           })
end
unless Subject.exists?(:name => "Hardware")
  Subject.create({
             :name => "Hardware",
             :description => "Something about Hardware technical"
           })
end
unless Subject.exists?(:name => "Network")
  Subject.create({
             :name => "Network",
             :description => "Something about Network technical"
           })
end

# ========= remove old subject=========== 
subs = Subject.where("name in ('android', 'window', 'ios')")
subs.delete_all
require 'rails_helper'

RSpec.describe "profiles/index", :type => :view do
  before(:each) do
    assign(:profiles, [
      Profile.create!(
        :user_id => 1,
        :last_name => "Last Name",
        :first_name => "First Name",
        :birthday => "Birthday",
        :address => "Address",
        :avatar => "Avatar",
        :course => "Course",
        :gender => 2,
        :resume => "MyText"
      ),
      Profile.create!(
        :user_id => 1,
        :last_name => "Last Name",
        :first_name => "First Name",
        :birthday => "Birthday",
        :address => "Address",
        :avatar => "Avatar",
        :course => "Course",
        :gender => 2,
        :resume => "MyText"
      )
    ])
  end

  it "renders a list of profiles" do
    render
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Last Name".to_s, :count => 2
    assert_select "tr>td", :text => "First Name".to_s, :count => 2
    assert_select "tr>td", :text => "Birthday".to_s, :count => 2
    assert_select "tr>td", :text => "Address".to_s, :count => 2
    assert_select "tr>td", :text => "Avatar".to_s, :count => 2
    assert_select "tr>td", :text => "Course".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end

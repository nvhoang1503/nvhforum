storage_file = File.join(Rails.root, "config", "storage.yml")
STORAGE_CONFIG = YAML.load_file(storage_file)[Rails.env].symbolize_keys
CarrierWave.configure do |config|
  if File.file?(storage_file)
    storage = STORAGE_CONFIG.clone
  else
    storage = {
      :provider => ENV['provider'],
      :access_key => ENV['access_key'],
      :secret_access_key => ENV['secret_access_key'],
      # :bucket => ENV['bucket'],
      :directory => ENV['directory']}
  end
  config.storage = :fog
  config.fog_credentials = {
    :provider               => storage[:provider],
    :aws_access_key_id      => storage[:access_key],
    :aws_secret_access_key  => storage[:secret_access_key],
    # :aws_bucket             => storage[:bucket],
    :region                 => 'ap-southeast-1'
  }
  config.fog_public     = true
  config.fog_directory  = storage[:directory]
  config.fog_attributes = {'Cache-Control' => 'max-age=315576000'}

  # config.will_include_content_type = true
  # config.validate_filename_format = false
end

class Answer < ActiveRecord::Base
  belongs_to :user
  belongs_to :question
  has_many :attachments, dependent: :destroy

  accepts_nested_attributes_for :attachments, :reject_if => :all_blank , :allow_destroy => true


  STATUS ={
    :good     => 1,
    :spam     => 2,
    :normal   => 3
  }

end

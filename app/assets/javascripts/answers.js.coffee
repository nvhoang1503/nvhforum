class window.Answer
  constructor: ->
    @clickOnLinkAnswerFeedback()
    @clickOnCancelAnswerFeedback()
    @clickOnBtnPostFeedbackAnswer()


  clickOnLinkAnswerFeedback: ->
    $(document).on "click", ".btn_answer_feedback", ->
      $(this).closest(".answer_box").find(".frm_feedback_answer").removeClass('hide')
      return false

  clickOnCancelAnswerFeedback: ->
    $(document).on "click", ".btn_cancel_answer_feedback",  ->
      $(this).closest(".frm_feedback_answer").addClass('hide')
      return false

  clickOnBtnPostFeedbackAnswer: ->
    $(document).on "click", ".btn_post_answer_feedback", ->
      answer_question_id = $(this).closest(".frm_feedback_answer").find(".question_answer_id").val()
      # feedback_value = $("input[name=feedback_answer]").val()
      feedback_value = $("input:radio[name=feedback_answer]:checked" ).val()
      alert feedback_value
      $("#loading").removeClass "hidden"
      $.ajax
        type: "GET"
        url: "/answers/post_answer_feedback"
        data: {answer_question_id: answer_question_id, feedback_value: feedback_value}
        success: (data)->
          $("#loading").addClass "hidden"
          $("#answers_question").html data
        error: (error)->
          $("#loading").addClass "hidden"
          alert "Data error!"




      return false
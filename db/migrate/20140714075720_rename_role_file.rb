class RenameRoleFile < ActiveRecord::Migration
  def change
    remove_column :users, :rule
    add_column :users, :role, :integer, :default => User::ROLE[:member]
  end
end

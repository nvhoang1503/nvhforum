class Attachment < ActiveRecord::Base
  belongs_to :topic
  belongs_to :question
  belongs_to :answer

  mount_uploader :data, MyUploader

end

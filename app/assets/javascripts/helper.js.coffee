class window.Helper
  constructor: ->
    @autoHideFlash()

  autoHideFlash: ->
    func =-> $('#flash-panel').fadeOut('slow')
    window.setTimeout func, 5000

  initDatepicker: (elem) ->
    if elem
      target = $(elem)
      target.datepicker({
        id: target.attr('id')
        format: 'yyyy/mm/dd'
        weekStart: 1
        autoclose: true
        todayHighlight: true
        startView: 0
      }).on 'changeDate', ->
        if $.trim(target.val()) != ''
          func =-> target.focusout()
          window.setTimeout(func, 100)
      if target.length > 0
        target_id = target.prop('id')
        $('.datepicker').prop('id', "date-#{target_id}") if target_id.length > 0

  isEmail: (email) ->
    regex = /^([a-zA-Z0-9_+-\.])+\@(([a-zA-Z0-9])+\.)+([a-zA-Z0-9]{2,4})+$/
    if email.indexOf('..') > 0 or email.indexOf('.@') > 0
      return false
    return regex.test(email)
class Topic < ActiveRecord::Base
  
  belongs_to :user
  belongs_to :category
  has_many :questions, dependent: :destroy
  has_many :attachments, dependent: :destroy

  accepts_nested_attributes_for :attachments, :reject_if => :all_blank , :allow_destroy => true

  scope :anothers, lambda {|topic_id| where "id != ?", topic_id}
  scope :another_topic_in_category, lambda {|topic_id, category_id| where "id != ? and category_id = ?", topic_id, category_id}

  def self.top_question
    sql = " select t1.*
            from topics t1, (select q.topic_id as id, count(q.topic_id) as num_question
                            from topics t, questions q
                            where t.id = q.topic_id
                            group by q.topic_id
                            order by num_question desc
                            limit 1) as t2
            where t1.id = t2.id 
          "
    topic = Topic.find_by_sql(sql).first
    unless topic
      topic = Topic.order("created_at desc").first
    end
    topic
  end

  def self.top_question_on_category(category_id)
    sql = " select t1.*
            from topics t1, (select q.topic_id as id, count(q.topic_id) as num_question
                            from topics t, questions q
                            where t.id = q.topic_id
                            and t.category_id = #{category_id}
                            group by q.topic_id
                            order by num_question desc
                            limit 1) as t2
            where t1.id = t2.id 
          "
    topic = Topic.find_by_sql(sql).first
     unless topic
      topic = Topic.where("category_id = ? ",category_id).order("created_at desc").first
    end
    topic
  end

end

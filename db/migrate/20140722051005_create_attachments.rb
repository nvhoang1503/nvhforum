class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :name
      t.string :data
      t.integer :topic_id
      t.integer :question_id
      t.integer :answer_id

      t.timestamps
    end
  end
end

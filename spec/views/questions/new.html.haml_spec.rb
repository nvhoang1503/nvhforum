require 'rails_helper'

RSpec.describe "questions/new", :type => :view do
  before(:each) do
    assign(:question, Question.new(
      :topic_id => 1,
      :user_id => 1,
      :title => "MyString",
      :content => "MyText",
      :attachment => "MyString",
      :status => 1
    ))
  end

  it "renders new question form" do
    render

    assert_select "form[action=?][method=?]", questions_path, "post" do

      assert_select "input#question_topic_id[name=?]", "question[topic_id]"

      assert_select "input#question_user_id[name=?]", "question[user_id]"

      assert_select "input#question_title[name=?]", "question[title]"

      assert_select "textarea#question_content[name=?]", "question[content]"

      assert_select "input#question_attachment[name=?]", "question[attachment]"

      assert_select "input#question_status[name=?]", "question[status]"
    end
  end
end

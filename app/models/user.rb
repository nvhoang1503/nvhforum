class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :profile
  has_many :topics, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :questions, dependent: :destroy
  has_many :answers, dependent: :destroy

  ROLE ={
    :super_admin => 1,
    :admin => 2,
    :member => 3
  }

  ROLE_TEXT ={
    1 => "super_admin",
    2 => "admin",
    3 => "member"
  }

  def is_supper_admin
    if self.role == User::ROLE[:super_admin]
      true
    else
      false
    end    
  end

  def is_admin
    if self.role == User::ROLE[:admin]
      true
    else
      false
    end    
  end

  def is_member
    if self.role == User::ROLE[:member]
      true
    else
      false
    end    
  end

  def full_name
    if self.profile
      profile.first_name  + " "+ profile.last_name
    else
      self.email
    end      
  end

  def conection_number
    25    
  end

end
